
<?php include('partials-front/menu.php')?>

    <!-- CAtegories Section Starts Here -->
    <section class="categories">
        <div class="container">
            <h2 class="text-center">Istraži hranu</h2>

            <?php
            
            //Prikazi sve aktivne kategorije iz baze podataka
            $sql= "SELECT * FROM tbl_category WHERE active='Yes'";

            $res = mysqli_query($conn,$sql);

            $count = mysqli_num_rows($res);

            //Provjeri je li aktegorija dostupna
            if($count>0)
            {
                //Dostupna
                while($row=mysqli_fetch_assoc($res))
                {
                    //DOhvati vrijednsoti
                    $id = $row['id'];
                    $title= $row['title'];
                    $image_name = $row['image_name'];
                    ?>

                    <a href="<?php echo SITEURL;?>category-foods.php?category_id=<?php echo $id;?>">
                        <div class="box-3 float-container">
                            <?php 
                                if($image_name=="")
                                {
                                    //Slike nije distupna
                                    echo "<div class='error'>Image Not Found.</div>";
                                }
                                else
                                {
                                    //Slika dostupna
                                    ?>
                                        <img src="<?php echo SITEURL;?>images/category/<?php echo $image_name;?>" alt="Pizza" class="img-responsive img-curve">
                                    <?php
                                }
                            ?>
                            

                            <h3 class="float-text text-white"><?php echo $title;?></h3>
                        </div>
                     </a>

                    <?php
                }

            }
            else
            {
                //Nije dostupna
                echo "<div class='error'>Nema dostupnih kategorija.</div>";
            }
            
            ?>


            

           

            

            <div class="clearfix"></div>
        </div>
    </section>
    <!-- Categories Section Ends Here -->


    <?php include('partials-front/footer.php')?>