<?php include('partials-front/menu.php')?>

<?php

        //Provjeri je li ID dostupan
        if(isset($_GET['food_id']))
        {
            //Dohvati id 
            $food_id = $_GET['food_id'];

            //Dohvati detalje o odabranoj hrani
            $sql = "SELECT * FROM tbl_food WHERE id=$food_id";

            $res = mysqli_query($conn,$sql);
            //Izbroj redove
            $count = mysqli_num_rows($res);
            //Provjeri dostupnost hrane
            if($count==1)
            {
                //Imamo podatke
                //Dohvati ih iz baze podataka
                $row=mysqli_fetch_assoc($res);

                $title= $row['title'];
                $price = $row['price'];
                $image_name = $row['image_name'];

            }
            else
            {
                //hrana nije dostupna
                header('location:'.SITEURL);
            }
        }
        else
        {
            //redirect do pocetne stranice
            header('location:'.SITEURL);
        }

?>

    <!-- fOOD sEARCH Section Starts Here -->
    <section class="food-search">
        <div class="container">
            
            <h2 class="text-center text-white">Ispunite obrazac kako biste potvrdili Vašu narudžbu.</h2>

            <form action="" method="POST" class="order">
                <fieldset>
                    <legend>Odabrana hrana</legend>

                    <div class="food-menu-img">
                    <?php
                    //Procjeri dostupnost slike
                        if($image_name=="")
                        {
                            //Slika nije dostupna
                            echo "<div class='error'>Image not available.</div>";

                        }
                        else
                        {
                            ?>
                            <img src="<?php echo SITEURL;?>images/food/<?php echo $image_name;?>" alt="Chicke Hawain Pizza" class="img-responsive img-curve">
                            <?php

                        }
                    ?>
                        
                    </div>
    
                    <div class="food-menu-desc">
                        <h3><?php echo $title;?></h3>
                        <input type="hidden" name="food" value="<?php echo $title;?>">

                        <p class="food-price"><?php echo $price;?> kn</p>
                        <input type="hidden" name="price" value="<?php echo $price;?>">

                        <div class="order-label">Količina</div>
                        <input type="number" name="qty" class="input-responsive" value="1" required>
                        
                    </div>

                </fieldset>
                
                <fieldset>
                    <legend>Delivery Details</legend>
                    <div class="order-label">Ime i Prezime</div>
                    <input type="text" name="full-name" placeholder="E.g. Ilija Opačak" class="input-responsive" required>

                    <div class="order-label">Broj telefona</div>
                    <input type="tel" name="contact" placeholder="E.g. +385 92 327 7403" class="input-responsive" required>

                    <div class="order-label">Email</div>
                    <input type="email" name="email" placeholder="E.g. hi@gmail.com" class="input-responsive" required>

                    <div class="order-label">Adresa</div>
                    <textarea name="address" rows="10" placeholder="E.g. Ulica, Grad, Poštanski broj" class="input-responsive" required></textarea>

                    <input type="submit" name="submit" value="Potvrdi narudžbu" class="btn btn-primary">
                </fieldset>

            </form>

            <?php
            
                //Provjeri je submit gumb kliknut
                if(isset($_POST['submit']))
                {
                    //Dohvati sve podatke iz forme
                    $food = $_POST['food'];
                    $price = $_POST['price'];
                    $qty = $_POST['qty'];

                    $total = $price * $qty; 

                    $order_date = date("Y-m-d h:i:sa");

                    $status = "Ordered"; //Ordered, On Delivery, Delivered, Cancelled

                    $customer_name = $_POST['full-name'];
                    $customer_contact = $_POST['contact'];
                    $customer_email = $_POST['email'];
                    $customer_address = $_POST['address'];


                    //Pohrani narudzbu u bazu
               
                    $sql2 = "INSERT INTO tbl_order SET
                        food = '$food',
                        price = $price,
                        qty = $qty,
                        total = $total,
                        order_date = '$order_date',
                        status = '$status',
                        customer_name = '$customer_name',
                        customer_contact = '$customer_contact',
                        customer_email = '$customer_email',
                        customer_address = '$customer_address'
                    ";
                    
                    //echo $sql2;die();
                    //izvrsi query
                    $res2 = mysqli_query($conn, $sql2);

                    //Provjeri je li query uspjesno izvrsen
                    if($res2==true)
                    {
                        //Narudzba zaprimljena
                        $_SESSION['order'] ="<div class='success text-center'>Uspješna narudžba.</div>";
                        header('location:'.SITEURL);
                    }
                    else
                    {
                        //Narudzba nije zaprimljena
                        $_SESSION['order'] ="<div class='error' text-center>Neuspješna narudžba.</div>";
                        header('location:'.SITEURL);
                    }

                }
            
            ?>

        </div>
    </section>
    <!-- fOOD sEARCH Section Ends Here -->

    <?php include('partials-front/footer.php')?>
