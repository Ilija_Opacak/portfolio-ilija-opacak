<?php include('partials-front/menu.php')?>

<?php

    //Provjeri je li id proslijeden ili ne
    if(isset($_GET['category_id']))
    {
        //ID kategorije je postavljen
        $category_id = $_GET['category_id'];
        
        $sql = "SELECT title FROM tbl_category WHERE id=$category_id";

        $res = mysqli_query($conn,$sql);

        //Dohvati vrijednosst iz base

        $row = mysqli_fetch_assoc($res);
        //Dohvati naslov
        $category_title = $row['title'];
    }
    else
    {
        //Kategorija nije proslijedena, redirect na pocetnu stranicu
        header('location:'.SITEURL);
    }

?>

    <!-- fOOD sEARCH Section Starts Here -->
    <section class="food-search text-center">
        <div class="container">
            
            <h2>Hrana koju pretražujete: <a href="#" class="text-white">"<?php echo $category_title;?>"</a></h2>

        </div>
    </section>
    <!-- fOOD sEARCH Section Ends Here -->



    <!-- fOOD MEnu Section Starts Here -->
    <section class="food-menu">
        <div class="container">
            <h2 class="text-center">Meni hrane</h2>


            <?php
            
                //Sql query za dohvacanje vrijednosti po kategoriji id-u
                $sql2 = "SELECT * FROM tbl_food WHERE category_id=$category_id";

                //Izvrsi query
                $res2 = mysqli_query($conn, $sql2);

                $count2 = mysqli_num_rows($res2);

                //Provjeri dostupnost hrane
                if($count2>0)
                {
                    //Dostupna
                    while($row2=mysqli_fetch_assoc($res2))
                    {
                        $id=$row2['id'];
                        $title = $row2['title'];
                        $price = $row2['price'];
                        $description = $row2['description'];
                        $image_name= $row2['image_name'];
                        ?>

                            <div class="food-menu-box">
                                <div class="food-menu-img">
                                    <?php
                                        if($image_name=="")
                                        {
                                            //iNije dostupna
                                            echo "<div class='error'>Image not Available-</div>";
                                        }
                                        else
                                        {
                                            //Dostupna
                                            ?>
                                            <img src="<?php echo SITEURL;?>images/food/<?php echo $image_name;?>" alt="Chicke Hawain Pizza" class="img-responsive img-curve">
                                            <?php
                                        }
                                    ?>
                                    
                                </div>

                                <div class="food-menu-desc">
                                    <h4><?php echo $title;?></h4>
                                    <p class="food-price">$<?php echo $price;?></p>
                                    <p class="food-detail">
                                        <?php echo $description;?>
                                    </p>
                                    <br>

                                    <a href="<?php echo SITEURL;?>order.php?food_id=<?php echo $id;?>" class="btn btn-primary">Order Now</a>
                                </div>
                            </div>

                        <?php
                    }
                }
                else
                {
                    //Hrana nije dostupna
                    echo "<div class='error'>Hrana nije dostupna</div>";
                }

            ?>
           

            


            <div class="clearfix"></div>

            

        </div>

    </section>
    <!-- fOOD Menu Section Ends Here -->

    <?php include('partials-front/footer.php')?>