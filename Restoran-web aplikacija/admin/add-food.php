<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Dodaj Hranu</h1>

            <br><br>

            <?php
            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset ($_SESSION['upload']);
            }
            ?>

            <form action="" method="POST" enctype="multipart/form-data">

                    <table class="tbl-30">
                        <tr>
                            <td>Title:</td>
                            <td>
                                <input type="text" name="title" placeholder="Naziv hrane">
                            </td>
                        </tr>

                        <tr>
                            <td>Opis:</td>
                            <td>
                                <textarea name="description"  cols="25" rows="5" placeholder="Opis hrane"></textarea>
                            </td>
                        </tr>

                        <tr>
                        <td>Cijena:</td>
                        <td>
                            <input type="number" name="price">
                        </td>
                        </tr>

                        <tr>
                        <td>Odaberi sliku:</td>
                        <td>
                            <input type="file" name="image" >
                        </td>
                        </tr>

                        <tr>
                            <td>Kategorija:</td>
                            <td>
                                <select name="category">

                                    <?php
                                        //Kreiraj PHP kod za prikaz kategorije iz baze
                                        //1. Kreiranje sql querya za odabir svih aktivnih kategorija
                                        $sql = "SELECT * FROM tbl_category WHERE active='Yes' ";
                                        //Izvrši query
                                        $res= mysqli_query($conn,$sql);

                                        //Izbroji redove da mozemo provjeriti jesu li dohvacene kategorije
                                        $count= mysqli_num_rows($res);
                                        //Ako je count veci od 0 imamo kategorija u bazi
                                        if($count>0)
                                        {
                                            //Imamo kategorije
                                            while($row=mysqli_fetch_assoc($res))
                                            {
                                                //Dohvati detalje kategorija
                                                $id = $row['id'];
                                                $title = $row['title'];
                                                ?>

                                                <option value="<?php echo $id?>"><?php echo $title?></option>

                                                <?php

                                            }
                                        }
                                        else
                                        {
                                            //Nemamo kategorije
                                            ?>
                                            <option value="0">No Categories Found</option>
                                            <?php 
                                        }

                                        
                                        
                                    ?>

                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Istaknut:</td>
                            <td>
                                <input type="radio" value="Yes" name="featured"> Da
                                <input type="radio" value="No" name="featured"> Ne
                            </td>
                        </tr>

                        <tr>
                            <td>Aktivan:</td>
                            <td>
                                <input type="radio" value="Yes" name="active"> Da
                                <input type="radio" value="No" name="active"> Ne
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <input type="submit" name="submit" value="Dodaj hranu" class="btn-secondary">
                            </td>
                        </tr>
                    </table>

            </form>

                <?php

                    //Provjeri je li gumb pritisnut
                    if(isset($_POST['submit']))
                    {
                        //Dodaj hranu u bazu podataka
                        //echo "Submit";
                        //1. Dohvati podatke iz forme
                        $title = $_POST['title'];
                        $description = $_POST['description'];
                        $price = $_POST['price'];
                        $category = $_POST['category'];
                        
                        //Provjeri jesu li postavljene vrijednosti na istaknutom i aktivnom radio buttonu
                        if(isset($_POST['featured']))
                        {
                            $featured = $_POST['featured'];
                        }
                        else
                        {
                            $featured = "No"; //Postavljanje defaultne vrijednosti
                        }

                        if(isset($_POST['active']))
                        {
                            $active = $_POST['active'];
                        }
                        else
                        {
                            $active = "No"; //Postavljanje defaultne vrijednosti
                        }

                        //2. Upload slike ako je odabrana
                        //Provjeri je li odabrana i uploadaj samo ako je odabrana
                        if(isset($_FILES['image']['name']))
                        {
                            //Dohvati detalje o slici
                            $image_name = $_FILES['image']['name'];

                            //Provjeri je li slika odabrana te uploadaj samo ako je
                            if($image_name!="")
                            {
                                //Slika odabrana
                                //A. Preimenuj sliku
                                // Dohvati extenziju
                                $ext = end(explode('.', $image_name));

                                //Preimenuj sliku
                                $image_name = "Food-name-".rand(0000,9999).".".$ext; 

                                //B. Upload sliku
                                //Dohvati Src Path i Destination path

                                //Source path je trenutna lokacija slike
                                $src = $_FILES['image']['tmp_name'];

                                //Destination path je lokacija gdje cemo pohraniti sliku
                                $dst = "../images/food/".$image_name;
                                //Uploadaj sliku
                                $upload = move_uploaded_file($src,$dst);

                                //Provjeri je li uploadana
                                if($upload==false)
                                {
                                    //Neuspješno uploadanje slike
                                    //Redirect do Add food stranice sa error porukom
                                    $_SESSION['upload']="<div class='error'>Failed to Upload image</div>";
                                    header('location:'.SITEURL.'admin/add-food.php');
                                    //Prekini proces
                                    die();
                                }
                                
                            }

                        }
                        else
                        {
                            $image_name = ""; //Postavljanje defaultne vrijednosti na prazno
                        }

                        //3. Pohrana u bazu
                        
                        //Kreiranje querija za pohranu hrane
                        $sql2 = "INSERT INTO tbl_food SET 
                            title = '$title',
                            description = '$description',
                            price = $price,
                            image_name = '$image_name',
                            category_id = $category,
                            featured = '$featured',
                            active = '$active'
                        ";
                        //Izvrši query
                        $res2 = mysqli_query($conn,$sql2);
                         //4. Redirect sa porukom do Manage Food stranice
                        //Provjeri jesu li podaci pohranjeni

                        if($res2==true)
                        {
                            //Podaci pohranjeni uspješno
                            $_SESSION['add']="<div class='success'>Hrana uspješno dodana!</div>";
                            header('location:'.SITEURL.'admin/manage-food.php');
                        }
                        else
                        {
                            //Neuspješno
                            $_SESSION['add']="<div class='error'>Hrana nije uspješno dodana!</div>";
                            header('location:'.SITEURL.'admin/manage-food.php');
                        }

                       
                    }

                ?>


    </div>
</div>

<?php include('partials/footer.php');?>