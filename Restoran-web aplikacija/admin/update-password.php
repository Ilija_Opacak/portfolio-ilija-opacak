<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Promijeni zaporku</h1>
        <br><br>

        <?php
            if(isset($_GET['id']))
            {
                $id=$_GET['id'];
            }
        ?>

        <form action="" method="POST">

        <table class="tbl-30">
            <tr>
                <td>Trenutna zaporka: </td>
                <td>
                    <input type="password" name="current_password" placeholder="Old password">
                </td>

            </tr>

            <tr>
                <td>Nova zaporka:</td>
                <td>
                <input type="password" name="new_password" placeholder="New Password">
                </td>
            </tr>
            <tr>
                <td>Potvrdi zaporku: </td>
                <td>
                    <input type="password" name="confirm_password" placeholder="Confirm Password">
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <input type="submit" name="submit" value="Promijeni zaporku" class="btn-secondary">

                </td>
            </tr>

        </table>
        </form>
    </div>
</div>

<?php

//Provjeri je li submit gumb kliknut ili ne
if(isset($_POST['submit']))
{
    //echo "clicked";

    //1. Dohvati podatke iz forme
    $id=$_POST['id'];
    $current_password = md5 ($_POST['current_password']);
    $new_password = md5($_POST['new_password']);
    $confirm_password = md5($_POST['confirm_password']);

    //2.Provjeri je li postoji user s tim podacima
    $sql = "SELECT * FROM tbl_admin WHERE id=$id AND password='$current_password'";

    //Izvrsi query
    $res = mysqli_query($conn, $sql);
    if($res==true)
    {
        //Provjeri jesu li podaci dostupni
        $count= mysqli_num_rows($res);
        if($count==1)
        {
            //User postoji i postoji mogucnost promijene zaporke
            //echo "User found";
            //Provjeri podudaranost zaporki
            if($new_password==$confirm_password)
            {
                //Azuriraj zaporku
                //echo "Password match";
                $sql2 = "UPDATE tbl_admin SET 
                    password='$new_password'
                    WHERE id=$id;
                ";
                //Izvrsi query
                $res2 = mysqli_query($conn,$sql2);

                //Provjeri je li se Query izvrsio
                if($res2==true)
                {
                    //Prikazi poruku o uspješnosti
                    $_SESSION['change-pwd']="<div class='success'>Zaporka uspješno promijenjena!</div>";
                    //Redirect 
                    header("location:".SITEURL.'admin/manage-admin.php');
                }
                else
                {
                    //Prikazi error poruku
                    $_SESSION['change-pwd']="<div class='error'>Zaporka nije uspješno promijenjena!</div>";
                    //Redirect 
                    header("location:".SITEURL.'admin/manage-admin.php');
                }
               
            }
            else
            {
                $_SESSION['pwd-not-match']="<div class='error'>Password does not match</div>";
                //Redirect the user
                header("location:".SITEURL.'admin/manage-admin.php');
            }

        }
        else
        {
            //User does not exist set message and redirect
            $_SESSION['user-not-found']="<div class='error'>User not Found</div>";
            //Redirect the user
            header("location:".SITEURL.'admin/manage-admin.php');
        }
    }

    //3. Check whether the New Password and Confirm Password Match or not

    //4. Change Password if all above is true
}

?>


<?php include('partials/footer.php');?>
