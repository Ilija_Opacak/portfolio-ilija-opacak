<?php include('partials/menu.php');?>

<div class="main-content">
<div class="wrapper">

    <h1>Kategorije</h1>

    <br/><br /><br />

    <?php
            if(isset($_SESSION['add']))
            {
                echo $_SESSION['add'];
                unset ($_SESSION['add']);
            }

            if(isset($_SESSION['remove']))
            {
                echo $_SESSION['remove'];
                unset ($_SESSION['remove']);
            }

            if(isset($_SESSION['delete']))
            {
                echo $_SESSION['delete'];
                unset ($_SESSION['delete']);
            }

            if(isset($_SESSION['update']))
            {
                echo $_SESSION['update'];
                unset ($_SESSION['update']);
            }

            if(isset($_SESSION['no-category-found']))
            {
                echo $_SESSION['no-category-found'];
                unset ($_SESSION['no-category-found']);
            }

            if(isset($_SESSION['update1']))
            {
                echo $_SESSION['update1'];
                unset ($_SESSION['update1']);
            }

            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset ($_SESSION['upload']);
            }

            if(isset($_SESSION['failed-remove']))
            {
                echo $_SESSION['failed-remove'];
                unset ($_SESSION['failed-remove']);
            }



        ?> <br>
                <!-- Button za dodavanje kategorije-->
                <a href="<?php echo SITEURL;?>admin/add-category.php" class="btn-primary" >Dodaj Kategoriju</a>
                <br /><br /><br />
                <table class="tbl-full">
                    <tr>
                        <th>S.N</th>
                        <th>Naslov</th>
                        <th>Slika</th>
                        <th>Istaknut</th>
                        <th>Aktivan</th>
                        <th>Akcije</th>
                    </tr>

                    <?php
                        //Query za dohvaćanje svih kategorija iz baze
                        $sql = "SELECT * FROM tbl_category";

                        //Izvrsavanje Querya
                        $res = mysqli_query($conn, $sql);

                        //Brojanje redova
                        $count = mysqli_num_rows($res);

                        //Inicijalizacija serijskog broja
                        $sn= 1;

                        //Provjeri imamo li podatke u bazi
                        if($count>0)
                        {
                            //Imamo podatke u bazi
                            //DOhvati podatke i display
                            while($row=mysqli_fetch_assoc($res))
                            {
                                $id = $row['id'];
                                $title = $row['title'];
                                $image_name = $row['image_name'];
                                $featured = $row['featured'];
                                $active = $row['active']
                                ?>

                                    <tr>
                                        <td><?php echo $sn++?> </td>
                                        <td><?php echo $title; ?></td>

                                        <td>
                                            <?php 
                                                //Provjeri dostupnost slike
                                                if($image_name!="")
                                                {
                                                    //Prikazi sliku
                                                    ?>

                                                        <img src="<?php echo SITEURL;?>images/category/<?php echo $image_name?>" width="100px" >

                                                    <?php
                                                }
                                                else
                                                {
                                                    //Prikazi poruku
                                                    echo "<div class='error'>Image Not Added</div>";
                                                }
                                            ?>
                                        </td>

                                        <td><?php echo $featured;?></td>
                                        <td><?php echo $active;?></td>
                                        <td>
                                            <a href="<?php echo SITEURL;?>admin/update-category.php?id=<?php echo $id;?>" class="btn-secondary">Ažuriraj kategoriju</a>
                                            <a href="<?php echo SITEURL;?>admin/delete-category.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name?>" class="btn-danger">Obriši kategoriju</a> 
                                        </td>
                                    </tr>


                                <?php 
                            }

                        }
                        else
                        {
                            //Nemamo podataka u bazi
                            //Prikazati cemo poruku u tablici
                            ?>

                            <tr>
                                <td colspan="6"><div class="error">Nije dodana niti jedna kategorija.</div></td>
                            </tr>

                            <?php
                        }

                    ?>

                 

                    
                </table>
</div>
</div>
<?php include('partials/footer.php');?>