<?php include('partials/menu.php');?>

<div class="main-content">
<div class="wrapper">

    <h1>Hrana</h1>

    <br/><br /><br />
    <?php
            if(isset($_SESSION['add']))
            {
                echo $_SESSION['add'];
                unset ($_SESSION['add']);
            }

            if(isset($_SESSION['delete']))
            {
                echo $_SESSION['delete'];
                unset ($_SESSION['delete']);
            }

            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset ($_SESSION['upload']);
            }

            if(isset($_SESSION['unauthorized']))
            {
                echo $_SESSION['unauthorized'];
                unset ($_SESSION['unauthorized']);
            }
            
            if(isset($_SESSION['update']))
            {
                echo $_SESSION['update'];
                unset ($_SESSION['update']);
            }


            


            
            ?>
            <br><br>
                <!-- Gumb za dodavanje hrane-->
                <a href="<?php echo SITEURL; ?>admin/add-food.php" class="btn-primary" >Dodaj Hranu</a>
                <br /><br /><br />
                <table class="tbl-full">
                    <tr>
                        <th>S.N</th>
                        <th>Naslov</th>
                        <th>Cijena</th>
                        <th>Slika</th>
                        <th>Istaknut</th>
                        <th>Aktivan</th>
                        <th>Akcije</th>
                    </tr>

                    <?php
                        //SQL query za dohvacanje podataka o hrani
                        $sql = "SELECT * FROM tbl_food";
                        //Izvrsenje querya
                        $res = mysqli_query($conn,$sql);
                        //Brojimo redove radi provjere ima li podataka u bazi
                        $count = mysqli_num_rows($res);
                        //Inicijalizacija serijskog broja
                        $sn=1;
                        if($count>0)
                        {
                            //Imamo hranu u bazi
                            //Dohvati sve podatke i prikazi ih
                            while($row=mysqli_fetch_assoc($res))
                            {
                                //DOhvati vrijednosti iz pojedinih stupaca
                                $id = $row['id'];
                                $title = $row['title'];
                                $price = $row['price'];
                                $image_name = $row['image_name'];
                                $featured = $row['featured'];
                                $active = $row['active'];
                                ?>
                                    <tr>
                                        <td><?php echo $sn++;?> </td>
                                        <td><?php echo $title;?></td>
                                        <td><?php echo $price;?> kn</td>
                                        <td>
                                            <?php 
                                            //Provjera imamo li sliku ili ne
                                            if($image_name=="")
                                            {
                                                //Nemamo sliku, prikazi error poruku
                                                echo "<div class='error'>Image Not Added</div>";
                                            }
                                            else
                                            {
                                                //Imamo sliku, prikazi sliku
                                                ?>
                                                <img src="<?php echo SITEURL;?>images/food/<?php echo $image_name;?>" width="100px">
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $featured;?></td>
                                        <td><?php echo $active;?></td>
                                        <td>
                                            <a href="<?php echo SITEURL; ?>admin/update-food.php?id=<?php echo $id;?>" class="btn-secondary">Ažuriraj Hranu</a>
                                            <a href="<?php echo SITEURL; ?>admin/delete-food.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name;?>" class="btn-danger">Obriši Hranu</a>
                                        </td>
                                    </tr>

                                <?php
                            }
                        }
                        else
                        {
                            //Nema hrane
                            echo "<tr><td colspan='7' class='error'> Hrana još nije dodana. </td></tr>";
                        }
                    ?>
                    

                   
                </table>
</div>
</div>
<?php include('partials/footer.php');?>