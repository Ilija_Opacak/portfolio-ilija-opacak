<?php 
    //Include constants.php za SITEURL
    include('../config/constants.php');
    //1. Uništi sesiju
    session_destroy(); //Unseta $_SESSION['user]

    //2. Redirect do Login stranice
    header('location:'.SITEURL.'admin/login.php');
?>