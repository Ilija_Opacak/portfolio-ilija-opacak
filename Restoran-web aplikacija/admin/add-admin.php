<?php include('partials/menu.php');?>


    <div class="main-content">
        <div class="wrapper">
            <h1>Dodaj Admina</h1>

            <br /><br/>

            <?php
                if(isset($_SESSION['add'])) //Provjeri je li sesija kreirana ili ne
                {
                    echo $_SESSION['add'];  //prikazi poruku sesije
                    unset($_SESSION['add']); //obrisi poruku sesije
                }
            ?>
            <form action="" method="POST">

                <table class="tbl-30">

                    <tr>
                        <td>Ime i Prezime:</td>

                        <td><input type="text" name="full_name" placeholder="Enter your name"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Username:</td>

                        <td><input type="text" name="username" placeholder="Enter your username"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Password:</td>

                        <td><input type="password" name="password" placeholder="Enter your password"> 
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input type="submit" name="submit" value="Dodaj admina" class="btn-secondary">
                        </td>
                    </tr>
                </table>

            </form>


        </div>


    </div>

<?php include('partials/footer.php');?>


<?php
//Procesiraj vrijednosti forme i pohrani ih u bazu

//Provjeri je li submit gumb stisnut ili ne

if(isset($_POST['submit']))
{
    // Clicked

    //echo"Button Clikced";

    //1. Dohvaćanje podataka iz forme

    $full_name = $_POST['full_name'];
    $username = $_POST['username'];
    $password = md5($_POST['password']);//Password Enkripcija sa MD5

    //2. SQL Query za pohranjivanje podataka u bazu
   $sql = "INSERT INTO tbl_admin SET 
        full_name='$full_name',
        username='$username',
        password='$password'
   ";
    //Izvršenje querya
   $res = mysqli_query($conn, $sql) or die(mysqli_error());

    //4. Provjeri je li se query izvršio ili nije te prikazi poruku
    if($res==TRUE)
    {
        //Podaci pohranjeni
        //echo "Data is inside";
        //Kreiranje sesije za prikaz poruke
        $_SESSION['add'] = "<div class='addsuccess'>Admin uspješno dodan!</div>";
        //redirect do manage admin stranice
        header("location:".SITEURL.'admin/manage-admin.php');
    }
    else
    {
        //Pohrana neuspješna
        //echo "Failed to insert";
        $_SESSION['add'] = "<div class='adderror'>Admin nije uspješno dodan!</div>";
        //redirect do Add admin stranice
        header("location:".SITEURL.'admin/add-admin.php');
    }

}


?>