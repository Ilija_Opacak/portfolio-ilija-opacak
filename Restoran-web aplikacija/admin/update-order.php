<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Ažuriraj Narudžbu</h1>
        <br><br>

<?php

    //Provjeri dostupnost id-a
    if(isset($_GET['id']))
    {
        //Dohvati sve detalje na osnovu id-a
        $id = $_GET['id'];
        
        $sql = "SELECT * FROM tbl_order WHERE id=$id";

        $res = mysqli_query($conn, $sql);

        $count = mysqli_num_rows($res);

        //Provjeri imamo li vrijednosti ili ne
        if($count==1)
        {
            //Podaci dostupni
            $row=mysqli_fetch_assoc($res);
            
            $food = $row['food'];
            $price = $row['price'];
            $qty = $row['qty'];
            $status = $row['status'];
            $customer_name = $row['customer_name'];
            $customer_contact = $row['customer_contact'];
            $customer_email = $row['customer_email'];
            $customer_address = $row['customer_address'];

        }
        else
        {
            //Nisu dostupni
            header('location:'.SITEURL.'admin/manage-order.php');
        }
        

    }
    else
    {
        //Redirect
        header('location:'.SITEURL.'admin/manage-order.php');
    }

?>


        <form action="" method="POST" >
        
            <table class="tbl-30">
                <tr>
                    <td>Naziv Hrane</td>
                    <td><b><?php echo $food;?></b></td>
                </tr>

                <tr>
                    <td>Cijena</td>
                    <td><b><?php echo $price?> kn</b></td>
                </tr>

                <tr>
                    <td>Količina</td>
                    <td>
                        <input type="number" name="qty" value="<?php echo $qty;?>">
                    </td>
                </tr>

                <tr>
                    <td>Status</td>
                    <td>
                    <select name="status" >
                        <option <?php if($status=="Ordered"){echo "selected";} ?>value="Ordered">Ordered</option>
                        <option <?php if($status=="On Delivery"){echo "selected";} ?>value="On Delivery">On Delivery</option>
                        <option <?php if($status=="Delivered"){echo "selected";} ?>value="Delivered">Delivered</option>
                        <option <?php if($status=="Cancelled"){echo "selected";} ?>value="Cancelled">Cancelled</option>
                    </select>
                    </td>
                </tr>

                <tr>
                    <td>Kupac</td>
                    <td>
                        <input type="text" name ="customer_name" value="<?php echo $customer_name;?>">
                    </td>
                </tr>

                <tr>
                    <td>Kontak Kupca</td>
                    <td>
                        <input type="text" name ="customer_contact" value="<?php echo $customer_contact;?>">
                    </td>
                </tr>

                <tr>
                    <td>Email Kupca</td>
                    <td>
                        <input type="text" name ="customer_email" value="<?php echo $customer_email;?>">
                    </td>
                </tr>
                <tr>
                    <td>Adresa Kupca</td>
                    <td>
                        <textarea name="customer_address"  cols="30" rows="5"><?php echo $customer_address;?></textarea>
                    </td>
                </tr>

                <tr>
                    <td colspan ="2">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input type="hidden" name="price" value="<?php echo $price;?>">
                        <input type="submit"  name="submit" value="Ažuriraj narudžbu" class="btn-secondary">
                    </td>
                </tr>

            </table>
        
        </form>

        <?php 
            //Provjeri je li gumb pritisnut
            if(isset($_POST['submit']))
            {
                //echo "clicked";
                //Dohvati sve podatke iz forme
                $id= $_POST['id'];
                $price= $_POST['price'];
                $qty= $_POST['qty'];

                $total= $price * $qty;

                $status = $_POST['status'];

                $customer_name = $_POST['customer_name'];
                $customer_contact = $_POST['customer_contact'];
                $customer_email = $_POST['customer_email'];
                $customer_address = $_POST['customer_address'];
                //Azuriraj podatke
                $sql2 = "UPDATE tbl_order SET
                    qty = $qty,
                    total = $total,
                    status = '$status',
                    customer_name = '$customer_name',
                    customer_contact = '$customer_contact',
                    customer_email = '$customer_email',
                    customer_address = '$customer_address'
                    WHERE id=$id
                ";
                    //Izvrsi query
                    $res2 = mysqli_query($conn,$sql2);  

                    //Provjeri je li azurirano
                    if($res2==true)
                    {
                        //Da
                        $_SESSION['update']="<div class='success'>Narudžba uspješno ažurirana.</div>";
                        header('location:'.SITEURL.'admin/manage-order.php');
                    }
                    else
                    {
                        //Ne
                        $_SESSION['update']="<div class='error'>Narudžba nije uspješno ažurirana.</div>";
                        header('location:'.SITEURL.'admin/manage-order.php');

                    }

                //redirect do manage order stranice sa porukom

            }
        ?>

    </div>
</div>



<?php include('partials/footer.php');?>