<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Update Category</h1>

        <br><br>

        <?php 

            //Provjeri je li id postavljen
            if(isset($_GET['id']))
            {
                //Dohvati id i sve ostale vrijednosti
                //echo "GETTING THE DATA";
                $id = $_GET['id'];
                //SQL Query za dohvacanje svih ostalih vrijednosti
                $sql = "SELECT * FROM tbl_category WHERE id=$id";

                //Izvrsenje querya
                $res=mysqli_query($conn,$sql);

                //Izbroji redove za provjeru postoje li podaci
                $count = mysqli_num_rows($res);

                if($count==1)
                {
                    //Dohvati sve podatke
                    $row = mysqli_fetch_assoc($res);
                    $title = $row['title'];
                    $current_image= $row['image_name'];
                    $featured = $row['featured'];
                    $active = $row ['active'];
                }
                else
                {
                //Redirect do manage category stranice s porukom
                $_SESSION['no-category-found'] = "<div class='error'>Ne postoji kategorija</div>";
                //Redirect do manage category stranice
                header('location:'.SITEURL.'admin/manage-category.php');
                }
            }
            else
            {
                $_SESSION['update'] = "<div class='error'>Azuriranje neuspješno!</div>";
                //Redirect do manage category stranice
                header('location:'.SITEURL.'admin/manage-category.php');
            }

        ?>

        <form action="" method="POST" enctype="multipart/form-data">
        <table class="tbl-30">
        <tr>
                    <td>Naslov:</td>
                    <td>
                        <input type="text" name="title" placeholder="Naslov Kategorije" value="<?php echo $title; ?>">
                    </td>
                </tr>

                <tr>
                    <td>Trenutna slika:</td>
                    <td>
                        <?php
                            if($current_image != "")
                            {
                                //Display the image
                                ?>
                                <img src="<?php echo SITEURL;?>images/category/<?php echo $current_image; ?>" width="150px">
                                <?php
                            }
                            else
                            {
                                //Display message
                                echo "<div class='error'>Slika nije dodana!</div>";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Nova slika:</td>
                    <td>
                        <input type="file" name="image">
                    </td>
                </tr>

                <tr>
                    <td>Istaknut:</td>
                    <td>
                        <input <?php if($featured=="Yes"){echo "checked";}?> type="radio" name="featured" value="Yes"> Yes

                        <input <?php if($featured=="No"){echo "checked";}?> type="radio" name="featured" value="No"> No
                    </td>
                </tr>

                <tr>
                    <td>Aktivan:</td>
                    <td>
                        <input <?php if($active=="Yes"){echo "checked";} ?> type="radio" name="active" value="Yes"> Yes

                        <input <?php if($active=="No"){echo "checked";} ?> type="radio" name="active" value="No"> No
                    </td>
                </tr>

                <tr>
                    <td>
                        <input type="hidden" name="current_image" value="<?php echo $current_image;?>">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <input type="submit" name="submit" value="Ažuriraj" class="btn-secondary">
                    </td>
                </tr>

        </table>
        </form>

        <?php

            if(isset($_POST['submit']))
            {
                //1. Dohvacanje svih vrijednosti iz forme
                $id= $_POST['id'];
                $title = $_POST['title'];
                $current_image = $_POST['current_image'];
                $featured = $_POST['featured'];
                $active = $_POST['active'];

                //2. Azuriranje nove slike ako je odabrana
                //PRovjeri je li odabrana
                if(isset($_FILES['image']['name']))
                {
                    //Dohvati detalje o slici
                    $image_name = $_FILES['image']['name'];
                    //Provjeri dostupnost slike
                    if($image_name !="")
                    {
                    //Slika dostupna
                    //A: Upload novu sliku 
                    $ext = end(explode('.', $image_name));

                    //Preimenuj sliku
                    $image_name = "Food_Category_".rand(000,999).'.'.$ext; //npr. Food_Category_123.jpg
                

                    $source_path = $_FILES['image']['tmp_name'];

                    $destination_path= "../images/category/".$image_name;

                    //Upload sliku
                    $upload = move_uploaded_file($source_path, $destination_path);

                    //Provjeri je li slika uploadana
                    
                    if($upload==false)
                    {
                        //Postavi poruku
                        $_SESSION['upload'] = "<div class='error'>Failed to upload image.</div>";
                        //Redirect do manage category stranice
                        header['location:'.SITEURL.'admin/manage-category.php'];
                        //Prekini proces
                        die();
                    }
                            //B:Obrisi trenutnu sliku ako postoji
                            if($current_image!="")
                            {
                                $remove_path = "../images/category/".$current_image;
                                $remove = unlink($remove_path);

                                //Provjeri je li obrisana
                                //Ako nije obrisana prikazi poruku i pprekini proces
                                if($remove==false)
                                {
                                    //Neuspjesno uklanjanje slike
                                    $_SESSION['failed-remove'] = "<div class='error'>Failed to remove Current Image</div>";
                                    header('location:'.SITEURL.'admin/manage-category.php'); 
                                    die();//prekini proces
                                }
                            }
                            

                        }
                        else
                        {
                            $image_name = $current_image;
                        }
                    }
                    else
                    {
                    
                        $image_name = $current_image;
                    }

                //3. Azuriraj u bazi
                $sql2 = "UPDATE tbl_category SET 
                    title='$title',
                    image_name='$image_name',
                    featured='$featured',
                    active='$active'
                    WHERE id=$id
                ";
                //Izvrsi Query
                $res2 = mysqli_query($conn, $sql2);


                //4. Redirect do manage category stranice s porukom
                //Provjeri je li query izvrsen 
                if($res2==true)
                {
                    //Kategorija azurirana
                    $_SESSION['update1'] = "<div class='success'>Kategorija uspješno ažurirana!</div>";
                    header('location:'.SITEURL.'admin/manage-category.php');
                }
                else
                {
                    //Nije azurirana
                    $_SESSION['update1'] = "<div class='error'>Kategorija nije uspješno ažurirana!</div>";
                    header('location:'.SITEURL.'admin/manage-category.php');
                }

            }

        ?>

    </div>
</div>

<?php include('partials/footer.php');?>