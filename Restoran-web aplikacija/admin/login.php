<?php include('../config/constants.php')?>

<html>
    <head>
        <title>Login - Sustav za narudzbu hrane</title>
        <link rel="stylesheet" href="../css/admin.css">
    </head>

    <body>

    <div class="login">
        <h1 class="text-center">Login</h1>
    <br><br>

    <?php
         if(isset($_SESSION['login']))
         {
            echo $_SESSION['login'];
            unset($_SESSION['login']);
         }

         if(isset($_SESSION['no-login-message']))
         {
            echo $_SESSION['no-login-message'];
            unset($_SESSION['no-login-message']);
         }
         
         
    ?><br><br>

        <!-- login form pocinje -->
        <form action="" method="POST" class="text-center">
            Username: <br>
            <input type="text" name="username" placeholder="Enter Username"><br><br>

            Password: <br>
            <input type="password" name="password" placeholder="Enter Password"><br><br>

            <input type="submit" name="submit" value="Login" class="btn-primary"><br><br>
        </form>
        <!-- login form zavrsava  -->

        <p class="text-center">Created By - Ilija Opačak</p>

    </div>


        

    
</html>

<?php 

    //Provjeri je li submit gumb kliknut
    if(isset($_POST['submit']))
    {
        //Process za Login
        //1. Dohvati podatke iz login forme
        $username = $_POST['username'];
        $password = md5($_POST['password']);

        //2. SQL za provjeru postoji li user u bazi podataka
        $sql = "SELECT * FROM tbl_admin WHERE username='$username'AND password='$password'";

        //3. Izvrsavanje Querya
        $res = mysqli_query($conn, $sql);

        //4. Prebroj redove za provjeru postojanosti usera
        $count = mysqli_num_rows($res);

        if($count==1)
        {
            //User postoji 
            $_SESSION['login'] = "<div class='success'>Login Successful.</div>";
            $_SESSION['user'] = $username; //Za provjeru je li korisnik ulogiran ili ne te ako nije unsetat ce ju

            //Redirect do manage admin stranice
            header('location:'.SITEURL.'admin/');
        }
        else
        {
            //User ne postoji
            $_SESSION['login'] = "<div class='error text-center'>Username or Password did not match.</div>";
            //redirect do login page
            header('location:'.SITEURL.'admin/login.php');
        }

    }

?>