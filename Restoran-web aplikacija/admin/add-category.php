<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Dodaj Kategoriju</h1>


        <br><br>

        <?php
            if(isset($_SESSION['add']))
            {
                echo $_SESSION['add'];
                unset ($_SESSION['add']);
            }

            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset ($_SESSION['upload']);
            }
        ?>

        <br><br>

        

        <!-- Dodaj kategoriju forma počinje -->
        <form action=""method="POST" enctype="multipart/form-data" >

            <table class="tbl-30">
                <tr>
                    <td>Title:</td>
                    <td>
                        <input type="text" name="title" placeholder="Naslov Kategorije">
                    </td>
                </tr>

                <tr>
                    <td>Odaberi sliku:</td>
                    <td>
                        <input type="file" name="image" >
                    </td>
                </tr>

                <tr>
                    <td>Istaknut:</td>
                    <td>
                        <input type="radio" name="featured" value="Yes"> Yes
                        <input type="radio" name="featured" value="No"> No
                    </td>
                </tr>

                <tr>
                    <td>Aktivan:</td>
                    <td>
                        <input type="radio" name="active" value="Yes"> Yes
                        <input type="radio" name="active" value="No"> No
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="submit" value="Dodaj Kategoriju" class="btn-secondary">
                    </td>
                </tr>

            </table>

        </form>
        <!-- Dodaj kategoriju forma završava -->
        
        <?php 
        //Provjeri je li submit button kliknut
        if(isset($_POST['submit']))
        {
            //echo "Clicked";

            //1. Dohvati vrijednosti iz forme Kategorija
            $title = $_POST['title'];
            
            //Za radio input type moramo provjeriti je li odabran 
            if(isset($_POST['featured']))
            {
                //Dohvati vrijednost iz forme
                $featured = $_POST['featured'];
            }
            else
            {
                //Postavni defaultnu vrijednost
                $featured="No";
            }
            if(isset($_POST['active']))
            {
                $active = $_POST['active'];
            }
            else
            {
                $active = "No";
            }

            //Provjeri je li slika odabrana ili nije, ako je promijeni joj ime
            //print_r($_FILES['image']);

            //die();//Podijeli code ovdje

            if(isset($_FILES['image']['name']))
            {
                //Upload sliku
                //Za upload slike trebamo njeno ime i destination i source putanje
                $image_name= $_FILES['image']['name'];

                //Upload sliku samo ako je odabrana
                if($image_name != "")
                {
                //Auto preimenuj sliku
                //Dohvati Extenziju slike (jpg,png,gif) npr. food1.jpg
                $ext = end(explode('.', $image_name));

                //Preimenuj sliku
                $image_name = "Food_Category_".rand(000,999).'.'.$ext; //npr. Food_Category_123.jpg
            

                $source_path = $_FILES['image']['tmp_name'];

                $destination_path= "../images/category/".$image_name;

                //Uploadnje slike
                $upload = move_uploaded_file($source_path, $destination_path);

                //Provjeri je li uspješno uploadana
                
                if($upload==false)
                {
                    //Postavi poruku
                    $_SESSION['upload'] = "<div class='error'>Failed to upload image.</div>";
                    //Redirect do Dodaj kategoriju stranice
                    header['location:'.SITEURL.'admin/add-category.php'];
                    //Prekini proces
                    die();
                }
            }
            }
            else
            {
                //Nemoj uploadat , postavi ime slike na prazno
                $image_name="";
            }

            //2. Kreiranje querya za pohranu kategorije u bazu
            $sql = "INSERT INTO tbl_category SET 
                title='$title',
                image_name='$image_name',
                featured='$featured',
                active='$active'
            ";
            //3. Izvrši query i pohrani podatke u bazu
            $res = mysqli_query($conn, $sql);

            //4. Provjeri uspješnost querya
            if($res==true)
            {
                //Query je uspješno izvršen
                $_SESSION['add'] = "<div class='success'>Kategorija uspješno dodana!</div>";
                //Redirect do Upravljanje kategorijama
                header('location:'.SITEURL.'admin/manage-category.php');
            }
            else
            {
                //Nije uspješno dodana kategorija
                $_SESSION['add'] = "<div class='error'>Kategorija nije uspješno dodana!</div>";
                //Redirect do manage category stranice
                header('location:'.SITEURL.'admin/add-category.php');
            }
        }
        ?>

    </div>
</div>



<?php include('partials/footer.php');?>
