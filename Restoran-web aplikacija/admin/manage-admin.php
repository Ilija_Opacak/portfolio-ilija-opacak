<?php include('partials/menu.php');?>

        <!-- main content section pocinje -->
        <div class="main-content">
            <div class="wrapper">
                <h1>Admin</h1>
                <br/>

                <?php 
                    if(isset($_SESSION['add']))
                    {
                        echo $_SESSION['add'];  //Prikaz poruke sesije
                        unset($_SESSION['add']); // Brisanje poruke sesije
                    }

                    if(isset($_SESSION['delete']))
                    {
                        echo $_SESSION['delete'];
                        unset($_SESSION['delete']);
                    }

                    if(isset($_SESSION['update']))
                    {
                        echo $_SESSION['update'];
                        unset($_SESSION['update']);
                    }

                    if(isset($_SESSION['user-not-found']))
                    {
                        echo $_SESSION['user-not-found'];
                        unset($_SESSION['user-not-found']);
                    }

                    if(isset($_SESSION['pwd-not-match']))
                    {
                        echo $_SESSION['pwd-not-match'];
                        unset($_SESSION['pwd-not-match']);
                    }
                    if(isset($_SESSION['change-pwd']))
                    {
                        echo $_SESSION['change-pwd'];
                        unset($_SESSION['change-pwd']);
                    }
                ?>
                <br/><br /><br />
               
                <!-- Gumb za dodavanje admina-->
                <a href="add-admin.php " class="btn-primary" >Dodaj Admina</a>
                <br /><br /><br />
                <table class="tbl-full">
                    <tr>
                        <th>S.N</th>  
                        <th>Ime i prezime</th>
                        <th>Username</th>
                        <th>Akcije</th>
                    </tr>

                    <?php
                        //Query za dphvacanje svih admina
                        $sql = "SELECT * FROM tbl_admin";
                        //Izvrsavanje querya
                        $res = mysqli_query($conn, $sql);
                        //Procjeri je li Query uspjesno izvrsen
                        if($res==TRUE)
                        {
                            
                            $count = mysqli_num_rows($res); //Funkcija za dohvacanje svih redova iz baze

                            $sn=1; //Kreiranje varijable i dodavanje vrijednosti

                            //Provjera broja redova
                            if($count>0)
                            {
                                //Imamo podatke
                                while($rows=mysqli_fetch_assoc($res))
                                {
                                    //koristenje shile petlje za dohvacanje svih podataka
                                    
                                    //dohvacanje pojedinih podataka 
                                    $id=$rows['id'];
                                    $full_name=$rows['full_name'];
                                    $username=$rows['username'];

                                    //Prikaz vrijednosti u tablici
                                    ?>

                                     <tr>
                                            <td><?php echo $sn++; ?> </td>
                                            <td><?php echo $full_name; ?></td>
                                            <td><?php echo $username; ?></td>
                                            <td>
                                            <a href="<?php echo SITEURL;?>admin/update-password.php?id=<?php echo $id ?>" class="btn-primary">Promijeni zaporku</a>
                                                <a href="<?php echo SITEURL;?>admin/update-admin.php?id=<?php echo $id ?>" class="btn-secondary">Ažuriraj Admina</a>
                                                <a href="<?php echo SITEURL;?>admin/delete-admin.php?id=<?php echo $id ?>" class="btn-danger">Obriši Admina</a>
                                        </td>
                                     </tr>

                                    <?php

                                }
                            }
                            else
                            {
                                //Nemamo podataka u bazi
                            }
                        }


                    ?>

                </table>

            </div>
        </div>
        <!-- main content section zavrsava -->

        <?php include('partials/footer.php');?>