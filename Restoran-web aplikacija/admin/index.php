<?php include ('partials/menu.php'); ?>

        <!-- main content section pocinje -->
        <div class="main-content">
            <div class="wrapper">
                <h1>NADZORNA PLOČA</h1>
                <br><br>

                <?php
                    if(isset($_SESSION['login']))
                    {
                        echo $_SESSION['login'];
                        unset($_SESSION['login']);
                    }
         
    ?>
        <br><br>
                <div class="col-4 text-center">

                <?php 
                $sql = "SELECT * FROM tbl_category";
                $res = mysqli_query($conn,$sql);
                $count = mysqli_num_rows($res);
                
                ?>
                    <h1><?php echo $count;?></h1>
                    <br />
                    Kategorije
                </div>
                <div class="col-4 text-center">

                <?php 
                $sql2 = "SELECT * FROM tbl_food";
                $res2 = mysqli_query($conn,$sql2);
                $count2 = mysqli_num_rows($res2);
                
                ?>
                    <h1><?php echo $count2;?></h1>
                    <br />
                    Hrana
                </div>
                <div class="col-4 text-center">
                <?php 
                $sql3 = "SELECT * FROM tbl_order";
                $res3 = mysqli_query($conn,$sql3);
                $count3 = mysqli_num_rows($res3);
                
                ?>
                    <h1><?php echo $count3;?></h1>
                    <br />
                    Ukupan broj narudžbi
                </div>
                <div class="col-4 text-center">

                    <?php 
                        //SQL query za dohvacanje ukupne zarade
                        //Aggregate funkijca u sql-u
                        $sql4 = "SELECT SUM(total) AS Total FROM tbl_order WHERE status='Delivered'";

                        //Izvrsi query
                        $res4 = mysqli_query($conn, $sql4);

                        //Dohvati vrijednost
                        $row4 = mysqli_fetch_assoc($res4);

                        $total = $row4 ['Total'];
                    ?>
                    <h1><?php echo $total;?> kn</h1>
                    <br />
                    Ukupna zarada:
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
        <!-- main content section zavrsava -->
<?php include('partials/footer.php');?>
     
