<?php 


include('../config/constants.php');
include('login-check.php');

?>



<html>
    <head>
        <title>Narudzba hrane - Naslovnica</title>

        <link rel="stylesheet" href="../css/admin.css">
    </head>
   
    <body>
        <!-- menu section starts -->
        <div class="menu text-center">
            <div class="wrapper">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="manage-admin.php">Admin</a></li>
                    <li><a href="manage-category.php">Kategorije</a></li>
                    <li><a href="manage-food.php">Hrana</a></li>
                    <li><a href="manage-order.php">Narudžbe</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
        </div>
        <!-- menu section ends -->
        