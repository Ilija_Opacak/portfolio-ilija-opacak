<?php 
    //Include constants.php datoteku
    include('../config/constants.php');

    //1. Dohvati ID admina kojeg cemo obrisati

    echo $id = $_GET['id'];

    //2. Kreiranje Querya za brisanje admina iz baze podataka

    $sql = "DELETE FROM tbl_admin WHERE id = $id";

    //Izvršenje Querya

    $res = mysqli_query($conn, $sql);

    //Provjera uspješnosti querya

    if($res==true)
    {
    //Query izvršen uspješno te je admin uklonjen
    //echo "Admin Deleted";
    //Kreiranje sesije sa uspješnom porukom
    $_SESSION['delete'] = "<div class='success'>Admin uspješno obrisan!</div>";
    //Redirect do Manage Admin stranice
    header('location:'.SITEURL.'admin/manage-admin.php');
    }
    else
    {
    //echo "Failed to Delete Admin";
    //Neuspješno uklanjanje admina

    $_SESSION['delete'] = "<div class='error'>Admin nije uspješno obrisan!</div>";
    header('location:'.SITEURL.'admin/manage-admin.php');

    }
   

?>