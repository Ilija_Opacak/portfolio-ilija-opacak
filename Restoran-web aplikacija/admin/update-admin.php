<?php include('partials/menu.php');?>


<div class="main-content">
        <div class="wrapper">
            <h1>Ažuriranje Admina</h1>

            <br/><br />

            <?php 
            
            //1 Dohvati id odabranog admina
            $id=$_GET['id'];

            //2 SQl query za dohvacanje svih detalja 
            $sql="SELECT * FROM tbl_admin WHERE id=$id";

            //3 Izvrsavanje querya
            $res=mysqli_query($conn,$sql);

            //4 Provjeri je li query izvrsen ili ne
            if($res==true)
            {
                //Provjeri jesu li podaci dostupni
                $count = mysqli_num_rows($res);
               
                if($count==1)
                {
                    $row=mysqli_fetch_assoc($res);

                    $full_name = $row['full_name'];
                    $username = $row['username'];
                }
                else
                {
                    //Redirect do Manage admin stranice
                    header('location:'.SITEURL.'admin/manage-admin.php');
                }
            }

            ?>
            <form action="" method="POST">
            <table class="tbl-30">
                    <tr>
                        <td>Ime i Prezime</td>
                        <td>
                            <input type="text" name="full_name" value="<?php echo $full_name; ?>">
                        </td>
                    </tr>

                    <tr>
                        <td>Username:</td>
                        <td><input type="text" name="username" value="<?php echo $username; ?>"></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="submit" name="submit" value="Ažuriraj admina" class="btn-secondary">

                        </td>
                    </tr>
                
            </table>
            </form>
        </div>
</div>

<?php

            //Provjeri je li submit gumb pritisnut
            if(isset($_POST['submit']))
            {
                //echo "Button Clicked";
                //Dohvacanje svih vrijednosti iz forme
                $id = $_POST['id'];
                $full_name = $_POST['full_name'];
                $username = $_POST['username'];

                //Sql query za azuriranje admina
                $sql = "UPDATE tbl_admin SET
                full_name='$full_name',
                username='$username'
                WHERE id='$id'
                ";

                //Izvrsenje Querya
                $res = mysqli_query($conn, $sql);

                //Provjeri je li Query uspio ili ne
                if($res==true)
                {
                    //Admin azuriran
                    $_SESSION['update'] = "<div class='success'>Admin uspješno Ažuriran.</div>";
                    //rediredct do manage admin stranice
                    header('location:'.SITEURL.'admin/manage-admin.php');
                }
                else
                {   //Nije azuriran
                    $_SESSION['update'] = "<div class='error'>Admin nije uspješno ažuriran.</div>";
                    //rediredct do manage admin stranice
                    header('location:'.SITEURL.'admin/manage-admin.php');
                    
                }
            }

?>


<?php include('partials/footer.php');?>