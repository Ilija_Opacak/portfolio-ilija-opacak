<?php
include('../config/constants.php');
//echo "Delete food page";

//Provjeri je li vrijednost proslijeđena
if(isset($_GET['id']) && isset($_GET['image_name']))
{
    
    //echo "Process to delete";

    //1. Dohvati ID i Image name
    $id = $_GET['id'];
    $image_name = $_GET['image_name'];


    //2. Obrisi sliku ako postoji
    
    if($image_name != "")
    {
        //Slika postoji i treba ju obrisati
        //dohvati putanju slike
        $path = "../images/food/".$image_name;

        //obrisi sliku iz direktorija
        $remove = unlink($path);

        //Provjeri je li slika obrisana
        if($remove==false)
        {
            //Nije
            $_SESSION['upload'] = "<div class='error'>Failed to Remove Image File</div>";
            header('location:'.SITEURL.'admin/manage-food.php');
            //Prekini proces
            die();
        }
    }
    //3. Brisanje hrane iz baze podataka
    $sql = "DELETE FROM tbl_food WHERE id=$id";
    //Izvrsenje querya
    $res = mysqli_query($conn,$sql);
    //Provjeri je li query uspjesno izvrsen
    //4. Redirect do manage food stranice s porukom
    if($res==true)
    {
        //Hrana obrisana
        $_SESSION['delete'] = "<div class='success'>Hrana uspješno obrisana!</div>";
        header('location:'.SITEURL.'admin/manage-food.php');
    }
    else
    {
        //Hrana nije obrisana
        $_SESSION['delete'] = "<div class='error'>Hrana nije uspješno obrisana!</div>";
        header('location:'.SITEURL.'admin/manage-food.php');
    }

    
}
else
{
    
    //echo "Redirect do manage food";
    $_SESSION['unauthorized']= "<div error='error'>Unauthorized Access</div>";
    header('location:'.SITEURL.'admin/manage-food.php');
}


?>