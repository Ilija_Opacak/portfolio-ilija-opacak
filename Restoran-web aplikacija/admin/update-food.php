<?php include('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Ažuriraj hranu</h1>

            <br><br>

            <?php
            //Provjeri je li id postavljen ili ne
            if(isset($_GET['id']))
            {
                //dohvati sve detalje
                $id = $_GET['id'];

                //sql query za dohvacanje hrane
                $sql2 = "SELECT * FROM tbl_food WHERE id=$id";
                //Izvrsenje Querya
                $res2 = mysqli_query($conn,$sql2);
                
                //Dohvati vrijednost baziranu na query izvrsenju
                $row2 = mysqli_fetch_assoc($res2);

                //Dohvati pojedine vrijednosti
                $title = $row2['title'];
                $description = $row2['description'];
                $price = $row2['price'];
                $current_image = $row2['image_name'];
                $current_category = $row2['category_id'];
                $featured = $row2['featured'];
                $active = $row2['active'];
            }
            else
            {
                //Redirect do manage food stranice
                header('location:'.SITEURL.'admin/manage-food.php');
            }
            ?>

            <form action="" method="POST" enctype="multipart/form-data">
                
                    <table class="tbl-30">
                            <tr>
                                <td>Naslov:</td>
                                <td>
                                    <input type="text" name="title" value="<?php echo $title;?>">
                                </td>
                            </tr>

                            <tr>
                                <td>Opis:</td>
                                <td>
                                    <textarea name="description"  cols="25" rows="5" placeholder="Opis hrane" ><?php echo $description; ?></textarea>
                                </td>
                            </tr>

                            <tr>
                            <td>Cijena:</td>
                            <td>
                                <input type="number" name="price" value="<?php echo $price?>">
                            </td>
                            </tr>

                            <tr>
                            <td>Trenutna slika:</td>
                            <td>
                                <?php
                                    if($current_image == "")
                                    {
                                        //Slika nije dostupna
                                        echo "<div class='error'>Slika nije dostupna.</div>";
                                    }
                                    else
                                    {
                                        //Slika dostupna
                                        ?>
                                        <img src="<?php echo SITEURL;?>images/food/<?php echo $current_image; ?>"  width="150px">
                                        <?php
                                    }
                                ?>
                            </td>
                            </tr>
                            <tr>
                            <td>Odaberite novu sliku:</td>
                            <td>
                                <input type="file" name="image">
                            </td>
                            </tr>

                            

                            <tr>
                                <td>Kategorija:</td>
                                <td>
                                <select name="category">

                                    <?php
                                    //Query za dohvacanje aktivnih kategorija
                                        $sql = "SELECT * FROM tbl_category WHERE active='yes'";
                                        //Izvrsenje querya
                                        $res = mysqli_query($conn,$sql);
                                        //Broji redove
                                        $count = mysqli_num_rows($res);
                                        //provjeri je li kategorija dostupna
                                        if($count>0)
                                        {
                                            //Dostupna
                                            while($row=mysqli_fetch_assoc($res))
                                            {
                                                $category_title = $row['title'];
                                                $category_id = $row['id'];

                                                //echo "<option value='$category_id'>$category_title</option>";
                                                ?>

                                                <option <?php if($current_category==$category_id){ echo "selected";}?> value="<?php echo $category_id; ?>"><?php echo $category_title?></option>

                                                <?php
                                            }
                                        }
                                        else
                                        {
                                            //Nije dostupna
                                            echo "<option value ='0'>Kategorija nije dostupna.</option>";
                                        }
                                    ?>

                                   
                                </select>
                            </tr> 

                            <tr>
                            <td>Istaknut:</td>
                            <td>
                                <input <?php if($featured=="Yes") {echo "checked";}?> type="radio" value="Yes" name="featured"> Yes
                                <input <?php if($featured=="No") {echo "checked";}?> type="radio" value="No" name="featured"> No
                            </td>
                        </tr>

                        <tr>
                            <td>Aktivan:</td>
                            <td>
                                <input <?php if($active=="Yes") {echo "checked";}?> type="radio" value="Yes" name="active"> Yes
                                <input <?php if($active=="No") {echo "checked";}?> type="radio" value="No" name="active"> No
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="id" value="<?php echo $id?>">
                                <input type="hidden" name="current_image" value="<?php echo $current_image;?>">
                                <input type="submit" name="submit" value="Ažuriraj hranu" class="btn-secondary">
                            </td>
                        </tr>
                 
 
                    </table>      
                </div>
                </div>

            </form>

                <?php
                     //Provjeri je li gumb pristisnut
                     if(isset($_POST['submit']))
                     {
                        //echo "Button clicked";
                        //1. Get all the details from the form
                        $id = $_POST['id'];
                        $title = $_POST['title'];
                        $description = $_POST['description'];
                        $price = $_POST['price'];
                        $current_image = $_POST['current_image'];
                        $category = $_POST['category'];

                        $featured = $_POST['featured'];
                        $active = $_POST['active'];
                        

                        //2. Upload the image if selected

                        //Check whether the upload is clicked or not
                        if(isset($_FILES['image']['name']))
                {
                    //Get the image details
                    $image_name = $_FILES['image']['name'];
                    //Check whether the image is available or not
                    if($image_name !="")
                    {
                    //Image available
                    //A: Upload the new image 
                    $ext = end(explode('.', $image_name));

                    //Rename the image
                    $image_name = "Food_Category_".rand(000,999).'.'.$ext; //npr. Food_Category_123.jpg
                

                    $source_path = $_FILES['image']['tmp_name'];

                    $destination_path= "../images/category/".$image_name;

                    //Finally upload the image
                    $upload = move_uploaded_file($source_path, $destination_path);

                    //Check whether the image is uploaded or not
                    //And if the image is not uploaded then we will stop the process and redirect with error message
                    if($upload==false)
                    {
                        //Set message
                        $_SESSION['upload'] = "<div class='error'>Failed to upload image.</div>";
                        //Redirect to manage category page
                        header['location:'.SITEURL.'admin/manage-category.php'];
                        //STOP the process
                        die();
                    }
                            //B:Remove the Current Image if available
                            if($current_image!="")
                            {
                                $remove_path = "../images/category/".$current_image;
                                $remove = unlink($remove_path);

                                //Check whether the image is removed or not
                                //If fail to remove then display message and stop the process
                                if($remove==false)
                                {
                                    //Failed to remove image
                                    $_SESSION['failed-remove'] = "<div class='error'>Failed to remove Current Image</div>";
                                    header('location:'.SITEURL.'admin/manage-category.php'); 
                                    die();//Stop process
                                }
                            }
                            

                        }
                        else
                        {
                            $image_name = $current_image;
                        }
                    }
                    else
                    {
                    
                        $image_name = $current_image;
                    }

                       

                        //4. Update the food in database
                            $sql3 =  "UPDATE tbl_food SET
                                title = '$title',
                                description = '$description',
                                price = $price,
                                image_name = '$image_name',
                                category_id = '$category',
                                featured = '$featured',
                                active = '$active'
                                WHERE id=$id
                            ";
                            //Execute the sql query
                            $res3 = mysqli_query($conn,$sql3);

                            if($res3==true)
                            {
                                //Query executed and food updated
                                $_SESSION['update'] = "<div class='success'>Hrana uspješno ažurirana!</div>";
                                header('location:'.SITEURL.'admin/manage-food.php');
                            }
                            else
                            {
                                //Failed to update Food
                                $_SESSION['update'] = "<div class='error'>Hrana nije uspješno ažurirana</div>";
                                header('location:'.SITEURL.'admin/manage-food.php');
                            }

                        //redirect
                        
                        
                    

                     }   
                ?>
<?php include('partials/footer.php');?>