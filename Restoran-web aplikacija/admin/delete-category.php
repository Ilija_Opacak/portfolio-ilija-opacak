
<?php
    //Include constants datoteku
    include('../config/constants.php');
    //echo"Delete Page";
    //Provjeri jesu li id i image name postavljeni
    if(isset($_GET['id']) AND isset($_GET['image_name']))
    {
        //Dohvati podatke i obriši
        //echo "Get Value and Delete";
        $id = $_GET['id'];
        $image_name = $_GET['image_name'];

        //Obriši fizičku sliku ako postoji
        if($image_name != "")
        {
            //slika postoji te ju obrisi
            $path = "../images/category/".$image_name;
            //obriši sliku
            $remove = unlink($path);
            // Ako nije uspješno brisanje slike prikazi poruku i redirect do manage category stranice
            if($remove==false)
            {
                //Postavi session poruku
                $_SESSION['remove'] = "<div class='error'>Failed to Remove Category Image.</div>";
                //redirect do manage category stranice
                header('location:'.SITEURl.'admin/manage-category.php');
                //prekini proces
                die();
            }
        }

        //Obrisi iz baze podataka
        //SQL Query za brisnaje podataka iz baze
        $sql = "DELETE FROM tbl_category WHERE id=$id";

        //Izvršavanje querya
        $res = mysqli_query($conn, $sql);

        //Provjeri jesu li podaci obrisani u bazi ili ne
        if($res==true)
        {
            //Postavi uspješnu poruku i redirect
            $_SESSION['delete'] = "<div class='success'>Kategorija uspješno uklonjena!</div>";

            header('location:'.SITEURL.'admin/manage-category.php');
        }
        else
        {
            //Postavi neuspješnu poruku i redirect
            $_SESSION['delete'] = "<div class='error'>Kategorija nije uspješno uklonjena!</div>";

            header('location:'.SITEURL.'admin/manage-category.php');
        }

        

    }
    else
    {
        //redirect do manage category stranice
        header('location:'.SITEURL.'admin/manage-category.php');
    }

?>