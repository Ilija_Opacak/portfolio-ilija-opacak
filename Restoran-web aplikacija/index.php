
    <?php include('partials-front/menu.php')?>

    <!-- fOOD sEARCH Section Starts Here -->
    <section class="food-search text-center">
        <div class="container">
            
            <form action="<?php echo SITEURL;?>food-search.php" method="POST">
                <input type="search" name="search" placeholder="Pretraži hranu.." required>
                <input type="submit" name="submit" value="Pretraži" class="btn btn-primary">
            </form>

        </div>
    </section>
    <!-- fOOD sEARCH Section Ends Here -->

            <?php 
                if(isset($_SESSION['order']))
                {
                    echo $_SESSION['order'];
                    unset ($_SESSION['order']);
                }
            ?>


    <!-- CAtegories Section Starts Here -->
    <section class="categories">
        <div class="container">
            <h2 class="text-center">Istraži hranu</h2>

            <?php 
                //Sql query za prikaz svih aktivnih i izdvojenih kategorija
                $sql = "SELECT * FROM tbl_category WHERE active='Yes' AND featured='Yes' LIMIT 3";

                $res = mysqli_query($conn,$sql);
                
                $count = mysqli_num_rows($res);
                if($count>0)
                {
                    //Kategorija dostupna
                    while($row=mysqli_fetch_assoc($res))
                    {
                        //Dohvati vrijednosti
                        $id = $row['id'];
                        $title = $row['title'];
                        $image_name = $row['image_name'];
                        ?>

                        <a href="<?php echo SITEURL;?>category-foods.php?category_id=<?php echo $id;?>">
                            <div class="box-3 float-container">
                                <?php 
                                    //Provjeri dostupnost slike
                                    if($image_name=="")
                                    {
                                        //Prikazi poruku
                                        echo "<div class='error'>Image not Available</div>";
                                    }
                                    else
                                    {
                                        //Slika dostupna
                                        ?>
                                            <img src="<?php echo SITEURL;?>images/category/<?php echo $image_name;?>" alt="Pizza" class="img-responsive img-curve">
                                        <?php
                                    }
                                ?>
                                

                                <h3 class="float-text text-white"><?php echo $title;?></h3>
                            </div>
            </a>

                        <?php
                    }
                }
                else
                {
                    //Nije dostupna niti jedna kategorija
                    echo "<div class='error'>Nije dostupna niti jedna kategorija.</div>";
                }

            ?>

            <div class="clearfix"></div>
        </div>
    </section>
    <!-- Categories Section Ends Here -->



    <!-- fOOD MEnu Section Starts Here -->
    <section class="food-menu">
        <div class="container">
            <h2 class="text-center">Meni Hrane</h2>

                <?php
                
                    //Dohvacanje hrane iz baze samo onih koji su aktivni i istaknuti
                    $sql2 = "SELECT * FROM tbl_food WHERE active='Yes' AND featured='Yes' LIMIT 6";

                    $res2 = mysqli_query($conn,$sql2);

                    $count2 = mysqli_num_rows($res2);
                    
                    //Provjeri dostupnost hrane
                    if($count2>0)
                    {
                        //Dostupna
                        while($row=mysqli_fetch_assoc($res2))
                        {
                            //Dohvacanje svih vriejdnosti
                            $id = $row['id'];
                            $title = $row['title'];
                            $price = $row['price'];
                            $description = $row['description'];
                            $image_name = $row['image_name'];
                            ?>

                            <div class="food-menu-box">
                                <div class="food-menu-img">
                                    <?php
                                        //Provjeri dostupnost slike
                                        if($image_name=="")
                                        {
                                            //Nije dostupna
                                            echo "<div class='error'>Image not Available.</div>";
                                        }
                                        else
                                        {
                                            //Dostupna
                                            ?>
                                                <img src="<?php echo SITEURL;?>images/food/<?php echo $image_name;?>" alt="Chicke Hawain Pizza" class="img-responsive img-curve">
                                            <?php
                                        }
                                    ?>
                                    
                                </div>

                                <div class="food-menu-desc">
                                    <h4><?php echo $title;?></h4>
                                    <p class="food-price"><?php echo $price;?> kn</p>
                                    <p class="food-detail">
                                        <?php echo $description;?>
                                    </p>
                                    <br>

                                    <a href="<?php echo SITEURL;?>order.php?food_id=<?php echo $id;?>" class="btn btn-primary">Naruči</a>
                                </div>
                            </div>

                            <?php

                        }
                    }
                    else
                    {
                        //Hrana nije dostupna
                        echo "<div class='error'>Hrana nije dostupna.</div>";
                    }
                
                ?>


            

            
            <div class="clearfix"></div>

            

        </div>

        <p class="text-center">
            <a href="<?php echo SITEURL;?>foods.php">See All Foods</a>
        </p>
    </section>
    <!-- fOOD Menu Section Ends Here -->

    <?php include('partials-front/footer.php')?>