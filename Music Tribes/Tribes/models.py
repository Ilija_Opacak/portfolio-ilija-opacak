from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image
from django.shortcuts import redirect 

class Tribes(models.Model):
    
    title = models.CharField(max_length=100)
    chief = models.ForeignKey(User, on_delete=models.CASCADE, related_name='chief')
    image= models.ImageField(upload_to='tribes_pics',  verbose_name="")
    genre = models.CharField(max_length=64)
    members = models.ManyToManyField(User, through='Membership')
    post_date = models.DateTimeField(default=timezone.now, blank=True)
    
    


    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('tribe:detail', kwargs={'pk': self.pk})



class Membership(models.Model):
    person = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Tribes, on_delete=models.CASCADE)
    date_joined = models.DateField()
    def __str__(self):
        return "Member: {} in Tribe: {}".format(self.person, self.group)  



class Playlist(models.Model):
   user = models.ForeignKey(User, on_delete=models.CASCADE )
   tribe = models.ForeignKey(Tribes, on_delete=models.CASCADE)
   name = models.CharField(max_length=64)
   description = models.CharField(max_length=256)
   date_created = models.DateTimeField(default=timezone.now, blank=True)
   
   def __str__(self):
      return "playlist: {}, from tribe: {}".format(self.name, self.tribe)   
   def get_absolute_url(self):
       tribe = self.tribe
       return reverse('tribe:detail', kwargs={'pk': tribe.pk})





class  Conversation(models.Model):
	sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
	receiver = models.ForeignKey(Tribes, on_delete=models.CASCADE, related_name='receiver')
	text = models.TextField()
	created_time = models.DateTimeField(auto_now_add=True,)

	def __str__(self):
		return "from {} to {}".format(self.sender, self.receiver)            


class Song(models.Model):
    title = models.CharField(max_length=150)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    artist = models.CharField(max_length=150)
    youtube_url = models.CharField(max_length=512)
    duration = models.FloatField(default=0.0)
    likes = models.ManyToManyField(User, related_name='like', default=0)

    def get_absolute_url(self):
        playlist = self.playlist
        return reverse('tribe:playlist', kwargs={'pk': playlist.pk})
    def __str__(self):
	    return " {}, song: {}".format(self.playlist, self.title)          

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    date_created = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return "user: {}".format(self.user)       

      