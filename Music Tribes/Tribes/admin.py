from django.contrib import admin
from .models import Membership, Tribes, Playlist, Conversation, Song, Comment
# Register your models here.


admin.site.register(Tribes)
admin.site.register(Membership)
admin.site.register(Playlist)
admin.site.register(Song)
admin.site.register(Comment)
from .models import *


class  ConversationAdmin(admin.ModelAdmin):
	list_display = ['sender', 'receiver', 'created_time']

admin.site.register(Conversation, ConversationAdmin)