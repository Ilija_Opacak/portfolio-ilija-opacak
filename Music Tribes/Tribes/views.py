from django.http import request
from PIL import Image
from Tribes.models import Membership, Tribes, Playlist, Song, Comment
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django import forms
import datetime
from django.views.generic import (
    CreateView,
    DetailView, 
    UpdateView,
    DeleteView,
    ListView, 
)
from django.shortcuts import  get_object_or_404, render, redirect
from django.db.models import Q
from .models import Conversation
from .forms import CommentForm




class TribeListView(ListView):
    model = Tribes
    template_name='Tribes/home.html' #<app>/<model>_<viewtype>.html
    context_object_name = 'tribes'
    


class TribeCreateView(LoginRequiredMixin, CreateView):
    model = Tribes
    fields = ['title', 'genre','image']

    def is_limit_reached(self):
        return Tribes.objects.filter(chief=self.request.user).count() >= 1

    def form_valid(self, form):
         
        
        if self.is_limit_reached():
            
            return HttpResponseRedirect(reverse('tribe:home') +'?limit=true')
        else:
            form.instance.chief = self.request.user
            
            return super().form_valid(form)




class TribeDetailView(DetailView):
    template_name = 'Tribes/tribes_detail.html'
    model = Tribes

    
    def post(self, request, *args, **kwargs):
	    receiver = get_object_or_404(Tribes, id=kwargs.get('pk'))
	    instance = self.model.objects.create(**{
				'sender': request.user,
				'receiver': receiver,
				'text': request.POST.get('text')
			})  
    def get_context_data(self,*args,**kwargs):
        tribe_id=self.object.id
        context=super(TribeDetailView, self).get_context_data(*args,**kwargs)
        context['object_list'] = Conversation.objects.filter(receiver_id=tribe_id)
        return context          

    



class TribeUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Tribes
    fields = ['title', 'genre', 'image']

    def form_valid(self, form):
        form.instance.chief = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post=self.get_object()
        if self.request.user == post.chief or self.request.user.is_staff:
            return True
        return False    




class TribeDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Tribes
    success_url= '/'

    def test_func(self):
        post=self.get_object()
        if self.request.user == post.chief or self.request.user.is_staff:
            return True
        return False 



class TribesForm(forms.ModelForm):

    class Meta:
        model = Tribes
        exclude = ['date_posted',]



class PlaylistCreateView(LoginRequiredMixin, CreateView):
    model = Playlist
    fields = ['name', 'description']

    def form_valid(self, form):
        tribe = Tribes.objects.get(id=self.kwargs['pk'])
        
        if self.request.user == tribe.chief or self.request.user.is_staff:
            form.instance.user = self.request.user
            form.instance.tribe_id = self.kwargs['pk']
            return super().form_valid(form)
        return HttpResponseRedirect(reverse('tribe:detail', kwargs={'pk': self.kwargs['pk']}) +'?limit4=true')



class PlaylistUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Playlist
    fields = ['name', 'description']

    def form_valid(self, form):
        return super().form_valid(form)

    def test_func(self):
        post=self.get_object()
        if self.request.user.is_staff or self.request.user == post.tribe.chief:
            return True
        return False   


class PlaylistDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Playlist
    

    def test_func(self):
        post=self.get_object()
        if self.request.user.is_staff or self.request.user == post.tribe.chief:
            return True
        return False 
    
    def get_success_url(self):
        return reverse('tribe:detail', kwargs={'pk': self.object.tribe.id}) 



class ConversationView(ListView):
	model = Conversation
	def get_context_data(self, *args, **kwargs):
        
		context = super().get_context_data(*args, **kwargs)
		context['tribes'] = Tribes.objects.all()
		return context

	def get_queryset(self):
		receiver = get_object_or_404(Tribes, id=self.kwargs.get('id'))
		messages = self.model.objects.filter(Q(sender=self.request.user, receiver=receiver) | Q(sender=self.request.user, receiver=receiver)).order_by('-created_time')

		return messages
	
	def post(self, request, *args, **kwargs):
	    receiver = get_object_or_404(Tribes, id=kwargs.get('id'))
	    instance = self.model.objects.create(**{
				'sender': request.user,
				'receiver': receiver,
				'text': request.POST.get('text')
			})
	    self.object_list = self.get_queryset()
	    context = self.get_context_data()
	    return HttpResponseRedirect(request.META.get('HTTP_REFERER')) 



class PlaylistDetailView(DetailView):
    
    model = Playlist
    

class SongCreateView(LoginRequiredMixin, CreateView):
    model = Song
    fields = ['title', 'artist','youtube_url','duration']

    def form_valid(self, form):
        playlist = Playlist.objects.get(id=self.kwargs['pk'])
        
        if self.request.user in playlist.tribe.members.all() or self.request.user == playlist.tribe.chief:
            song = form.save(commit=False)
            if song.youtube_url.startswith("https://www.youtube.com/"):
                form.instance.user = self.request.user
                form.instance.playlist_id = self.kwargs['pk']
                return super().form_valid(form)
            else: 
                return HttpResponseRedirect(reverse('tribe:playlist', kwargs={'pk': self.kwargs['pk']})  +'?limit2=true')
        return HttpResponseRedirect(reverse('tribe:playlist', kwargs={'pk': self.kwargs['pk']}) +'?limit3=true')    

class SongDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Song
    

    def test_func(self):
        post=self.get_object()
        if self.request.user.is_staff or self.request.user == post.playlist.tribe.chief or self.request.user == post.user:
            return True
        return False 
    
    def get_success_url(self):
        return reverse('tribe:playlist', kwargs={'pk': self.object.playlist.id})


def join(request,pk):
    
   
    new_obj = Tribes.objects.get(id=pk)
    new_obj.user = request.user 
    new_obj.save()
    if Membership.objects.filter(person=new_obj.user, group=new_obj).exists():
        return HttpResponseRedirect(reverse('tribe:home') )
    else:     
        membership = Membership.objects.create(person_id=new_obj.user.id, group = new_obj, date_joined=datetime.datetime.now())
        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



def leave(request,pk):
    filter = Membership.objects.filter(person_id=request.user.id, group_id=pk)
    filter.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



def kick(request,id1,id2):
    tribe = Tribes.objects.get(id=id2)
    if tribe.chief_id == request.user.id or request.user.is_staff:
        filter = Membership.objects.filter(person_id=id1, group_id=id2)
    
        filter.delete()
        return HttpResponseRedirect(reverse('tribe:detail', kwargs={'pk': tribe.id}) )    
    return HttpResponseRedirect(reverse('tribe:detail', kwargs={'pk': tribe.id}) +'?limit6=true')   


   

def delete_message(request,id1,id2,text):   
    tribe = Tribes.objects.get(id=id2)
    
    if tribe.chief_id == request.user.id or request.user.is_staff:
        filter = Conversation.objects.filter(sender_id=id1, receiver_id=id2, text=text)
    
        filter.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))  
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))  



def like(request, pk):
    song = get_object_or_404(Song, id=pk)
    if request.user in song.playlist.tribe.members.all() or request.user == song.playlist.tribe.chief:
        if song.likes.filter(id=request.user.id).exists():
            song.likes.remove(request.user)
        else:
            song.likes.add(request.user)
        return HttpResponseRedirect(reverse('tribe:playlist', kwargs={'pk': song.playlist.id}) ) 
    return HttpResponseRedirect(reverse('tribe:playlist', kwargs={'pk': song.playlist.id}) +'?limit5=true') 

def comment(request, tribe_id, playlist_id, song_id):
    tribe = get_object_or_404(Tribes, pk = tribe_id)
    playlist = get_object_or_404(Playlist, pk = playlist_id)
    song = get_object_or_404(Song, pk = song_id)
    songs = Song.objects.all().filter(playlist= playlist)
    comments = Comment.objects.all().filter(song=song)
    user = request.user
    form = CommentForm(request.POST)

    if request.method == 'POST' and  user in tribe.members.all() or user == tribe.chief or user.is_staff:
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.song = song
            comment.save()
            return redirect(reverse('tribe:playlist', kwargs={ 'pk': playlist.id}))
    
    context = {
        'tribe': tribe,
        'playlist': playlist,
        'songs' : songs,
        'comments': comments,
        'form' : form
    }
    
    return render(request, 'tribe:playlist', context)


def delete_comment(request, tribe_id, playlist_id, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    tribe = get_object_or_404(Tribes, pk = tribe_id)
    user = request.user 
    if user == comment.user or user.is_superuser or user == tribe.chief:
        comment.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER')) 
    return redirect(reverse('tribe:playlist', kwargs={ 'pk': playlist_id}) + '?limit7=true')