from django import forms
from django.forms import ModelForm
from .models import Conversation, Comment

class ConversationForm(forms.ModelForm):

	class Meta:
		model = Conversation
		fields = ['text']	

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']		