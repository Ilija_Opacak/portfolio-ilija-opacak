from django.urls import path
from . import views
from .views import (
    TribeCreateView,
    TribeDetailView, 
    TribeUpdateView,
    TribeDeleteView,
    TribeListView,
    PlaylistCreateView,
    PlaylistUpdateView,
    PlaylistDeleteView,
    ConversationView,
    PlaylistDetailView,
    SongCreateView,
    SongDeleteView
)


app_name= 'tribe'

urlpatterns = [
    path('', TribeListView.as_view(), name='home'),

    path('tribe/new/', TribeCreateView.as_view(), name='tribe-create'),
    path('chat/<int:id>/', ConversationView.as_view(), name='chat_conversation' ),
    path('tribe/<int:pk>/', TribeDetailView.as_view(), name='detail'),
    path('tribe/<int:pk>/update/', TribeUpdateView.as_view(), name='update'),
    path('tribe/<int:pk>/delete/', TribeDeleteView.as_view(), name='delete'),

    path('join/<int:pk>', views.join, name='join'),
    path('leave/<int:pk>', views.leave, name='leave'),
    path('kick/<int:id1>/<int:id2>/', views.kick, name='kick'),

    path('playlist/<int:pk>/new', PlaylistCreateView.as_view(), name='create-playlist'),
    path('playlist/<int:pk>/update/', PlaylistUpdateView.as_view(), name='update-playlist'),
    path('playlist/<int:pk>/delete/', PlaylistDeleteView.as_view(), name='delete-playlist'),
    path('tribe/playlist/<int:pk>/', PlaylistDetailView.as_view(), name='playlist'),

    path('deletemsg/<int:id1>/<int:id2>/<str:text>', views.delete_message, name='delete-msg'),

    path('playlist/song/new/<int:pk>', SongCreateView.as_view(), name='song-create'),
    path('song/<int:pk>/delete/', SongDeleteView.as_view(), name='song-delete'),
    path('song-like/<int:pk>', views.like, name="song-like"),
    
    path('comment/<int:tribe_id>/<int:playlist_id>/<int:song_id>', views.comment, name='comment'),	
    path('delete-comment/<int:tribe_id>/<int:playlist_id>/<int:comment_id>', views.delete_comment, name='delete-comment'),	
    
]

