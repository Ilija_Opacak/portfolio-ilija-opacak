# Portfolio-Ilija Opačak


Cramer Calculator

Web aplikacija za rješavanje sustava linearnih jednadžbi Cramerovom metodom. 
Dostupna na poveznici : http://www.etfos.unios.hr/~ilija.opacak/


Music Tribes

Društvena web aplikacija koja se zasniva na MVC frameworku. Omogućuje korisnicima kreiranje njihovog plemena te upravljanje članovima i playlistama tog plemena. Također svako pleme ima svoj chatbox koji je vidljiv samo članovima tog plemena.


Restoran

Web aplikacija koja omogućuje korisnicima narudžbu hrane, a adminu upravljanje osobljem, ponudom restorana i narudžbama.
