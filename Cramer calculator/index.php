
<!DOCTYPE html>
<meta charset="utf-8">

<html lang="en">

    <head>
        <link rel="stylesheet" href="style.css">
        
    </head>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
    
    
    <div id="header" class="menu ">
        <br>
    <img src="calculator.png" class="logo">

    <h1 ><a href="index.php" style="text-decoration: none; color:white;">Cramer kalkulator</a></h1>

    </div>
    <body>
    
    
        
    <?php
    $nepoznanice=2;
    if(isset($_POST['submit']))
    { 
        $nepoznanice = $_POST['nepoznanice'];
        //echo "clicked";
    }
           
    ?>
    
    <button  class="button" style="display:none; margin:auto;" id="povratak" onclick="povratak();">Nazad</button>
    <div  class="dodatno" style="display:none;" id="dodatno">Ovdje će biti prikazan prošireni oblik Vašeg rješenja nakon što riješite sustav!</div>
    
    <div class="wrapper text-center" id="pocetno">

    <div id="buttons" class="buttons">

    

    <p style="color:whitesmoke;font-size: 23px;"><b>Unesite broj jednadžbi</b></p>

    <form action="" method="POST">
        
        <input type="number" name=" nepoznanice" id="n"  value="<?php echo $nepoznanice;?>" min="2" max="100" >
        <br><br><br>
        <input type="submit" name="submit" value="Kreiraj matricu" class="button">
        <input onClick="window.location.reload();" value="Osvježi" class="button" style="width:70px;">
    </form> 
   
    <br><br>
    <button  class="button" id="ButtonSolve" >Izračunaj</button>
    <button  class="button" onclick="provjera()" >Provjeri</button>
    

    <br><br>


    
    <div class="rjesenja" id="rjesenja" ></div>
    <br><br>
    </div>
   

    
    <table id="example">
        <thead>
            <tr>
                
            <?php for($i=1;$i<=$nepoznanice;$i++){?><th>x<?php echo $i;};?></th>
            <th>b</th>
                
            </tr>
        </thead>
        <tbody>
        
            
            
        <?php for($i=0;$i<=$nepoznanice-1;$i++){?>
            <tr>
            <?php for($j=0;$j<=$nepoznanice-1 ;$j++){?>
            
            <td>0<?php }?></td>
            <td>0<?php }?></td>
            </tr>
            
        </tbody>
    </table>
    
    </div>
    
    <script src="./script2.js"></script>
    <script src="./script1.js"></script>

   


    </div>

    <footer>
    <br><br><br><br><br><br>
    <div id="footer" class="footer">


        
            <div  class="wrapper">
                <div style="width: 100px; margin: 0 auto;" ><a href="#" class="contbutton text-center"   id="contbutton"><b>Upute</b></a></div>
                    <p class="text-center">2021. Cramer kalkulator. Razvio - Ilija Opačak </p>
                    
                </div>
            </div>
            
    </footer>  
    </body> 
    <script>
     $(function(){
        $(window).scroll(function(){ $('#header,#footer,#buttons').css({'left': $(this).scrollLeft()});});
        
       
        
});

 </script> 
 <section class="contact">
            <div class="contact-content">
                <img src="close.png" alt="" class="close">
                <h2>O Cramerovom pravilu</h2><br>
                <div class="contacttext">
    <p>Kako biste riješili sustav linearnih jednadžbi pomoću Cramerovog pravila, morate učiniti sljedeće:
    <br><br><b>
    1.  Postavite proširenu matricu.
    <br><br>
    2.  Izračunajte determinantu glavne (kvadratne) matrice.
    <br><br>
    3.  Da biste pronašli 'i' -to rješenje sustava linearnih jednadžbi koristeći Cramerovo pravilo, zamijenite 'i -ti stupac glavne matrice vektorom rješenja i izračunajte njegovu determinantu. Zatim podijelite tu determinantu s glavnom - to je jedan dio skupa rješenja, određen pomoću Cramerovog pravila. Ponovite ovaj postupak za svaku varijablu.
    <br><br>
    4.  Ako je glavna determinanta nula, sustav linearnih jednadžbi je ili bez rješenja ili ima beskonačno mnogo rješenja. Nažalost, nemoguće je to točno provjeriti koristeći Cramerovo pravilo. 
    <br><br></b>
    Da biste razumjeli Cramerov algoritam , bolje unesite bilo koji primjer i ispitajte rješenje. </p>
                </div>
            </div>

    </section>






<script>
        window.onload = function(){
            document.getElementById("contbutton").addEventListener("click", function(){
            document.querySelector(".contact").style.display = "flex";
        })
        document.querySelector(".close").addEventListener("click", function(){
            document.querySelector(".contact").style.display = "none";
        })

    }
</script>



<script>
    function provjera() {
  var x = document.getElementById("pocetno");
  var y = document.getElementById("dodatno");
  var z = document.getElementById("povratak");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
    y.style.display = "block";
    z.style.display = "block";
    
  }
}
function povratak() {
  var x = document.getElementById("pocetno");
  var y = document.getElementById("dodatno");
  var z = document.getElementById("povratak");
  x.style.display="block";
  y.style.display="none";
  z.style.display="none";
}
</script>
</html>
    