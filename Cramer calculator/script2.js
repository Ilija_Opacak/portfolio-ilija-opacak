//Skripta koja računa nepoznanice cramerovim pravilom
var table = document.getElementById('example');
var nepoznanice = document.getElementById("n");  
var list =[];   
var k=0,l=0; 
var rjesenja =[];


 
        
        
    $(document).ready(function(){
    $('#ButtonSolve').click(function(){
     izracunaj();
     
    });
    });       

function izracunaj(){
    var myTableArray = [];
    var freeTerms = [];
    var matrix = [];
    var br2=1;
    $("table#example tr").each(function() {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
            tableData.each(function() { arrayOfThisRow.push($(this).text()); });
            myTableArray.push(arrayOfThisRow);
        }
    });
    

    for(var i=0;i<table.rows.length-1;i++){
        var j=table.rows.length-1;
        freeTerms [i] = myTableArray[i][j];
        
    }
    

    for(var i=0;i<table.rows.length-1;i++){
    for(var j=0;j<(table.rows.length-1);j++){
        matrix[i] = myTableArray[i];
        
        
    }}
    for(var i=0;i<table.rows.length-1;i++){
        matrix[i].splice(table.rows.length-1,i+1);
    }
    
    var dodatno=[];
    var determinants=[];
    var o=0;
    
    var result = cramersRule(matrix,freeTerms);
    console.log(result);
    for(var i=0;i<table.rows.length-1;i++){
        if(result[i] == Infinity || result[i] == -Infinity){
            o++;
            
        }
    }
    if(o!=0){
        
        window.alert("Determinanta matrice je jednaka "+ detr(matrix)+"." + "\nVaš sustav nema rješenja! ");
        document.getElementById('dodatno').innerHTML = 'Ovdje će biti prikazan prošireni oblik Vašeg rješenja nakon što riješite sustav!';
        document.getElementById('rjesenja').innerHTML = '';
        o=0;
    }else if(detr(matrix)==0) {
    
        window.alert("Determinanta matrice je jednaka 0. \nVaš sustav ima beskonačno mnogo rješenja!");
        document.getElementById('dodatno').innerHTML = 'Ovdje će biti prikazan prošireni oblik Vašeg rješenja nakon što riješite sustav!';
        document.getElementById('rjesenja').innerHTML = '';
        
    }
        
    
   
     
    /**
     * Compute Cramer's Rule
     * @param  {array} matrix    x,y,z, etc. terms
     * @param  {array} freeTerms
     * @return {array}           solution for x,y,z, etc.
     */
    function cramersRule(matrix,freeTerms) {
        var det = detr(matrix),
            returnArray = [],
            i,
            tmpMatrix;
            console.log(detr(matrix));    
        for(i=0; i < matrix[0].length; i++) {
            var tmpMatrix = insertInTerms(matrix, freeTerms,i)
            returnArray.push(detr(tmpMatrix)/det)
            determinants[i]=detr(tmpMatrix);
        }
        
        return returnArray;
    }
     
    /**
     * Inserts single dimensional array into
     * @param  {array} matrix multidimensional array to have ins inserted into
     * @param  {array} ins single dimensional array to be inserted vertically into matrix
     * @param  {array} at  zero based offset for ins to be inserted into matrix
     * @return {array}     New multidimensional array with ins replacing the at column in matrix
     */
    function insertInTerms(matrix, ins, at) {
        
        var tmpMatrix = clone(matrix),
            i;
        for(i=0; i < matrix.length; i++) {
            tmpMatrix[i][at] = ins[i];
        }
        
        console.log(tmpMatrix);
        console.log(detr(tmpMatrix));
        return tmpMatrix;
    }
    /**
     * Compute the determinate of a matrix.  No protection, assumes square matrix
     * function borrowed, and adapted from MIT Licensed numericjs library (www.numericjs.com)
     * @param  {array} m  Input Matrix (multidimensional array)
     * @return {number}   result rounded to 2 decimal
     */
    function detr(m) {
        
        var ret = 1,
            k,
            
            A=clone(m),
            n=m[0].length,
            alpha;
        
        
             
            
        for(var j =0; j < n-1; j++) {
            k=j;
            for(i=j+1;i<n;i++) { if(Math.abs(A[i][j]) > Math.abs(A[k][j])) { k = i; } }
            if(k !== j) {
                temp = A[k]; A[k] = A[j]; A[j] = temp;
                ret *= -1;
                
            }
            Aj = A[j];
            
            for(i=j+1;i<n;i++) {
                Ai = A[i];
                alpha = Ai[j]/Aj[j];
                
                for(k=j+1;k<n-1;k+=2) {
                    k1 = k+1;
                    Ai[k] -= Aj[k]*alpha;
                    Ai[k1] -= Aj[k1]*alpha;
                    
                }
                if(k!==n) { Ai[k] -= Aj[k]*alpha;}
                
            }
            if(Aj[j] === 0) { return 0; }
            ret *= Aj[j];

             
            }
            
        return Math.round(ret*A[j][j]*100)/100; "<br>" 
        
    }
     
    /**
     * Clone two dimensional Array using ECMAScript 5 map function and EcmaScript 3 slice
     * @param  {array} m Input matrix (multidimensional array) to clone
     * @return {array}   New matrix copy
     */
    function clone(m) {
        
        return m.map(function(a){return a.slice();});
    }
   isNaN(detr(matrix));
   isNaN(result);
 

   
   
   var br=0;
   var osnovna=detr(matrix);
   
    for(var i=0;i<table.rows.length-1;i++){
        if(freeTerms[i]==0){
         br++;
        }
        if(br==table.rows.length-1){
         window.alert("Unesite slobodne koeficijente kako bi ste uspješno rješili Vaš sustav!");
         document.getElementById('dodatno').innerHTML = 'Ovdje će biti prikazan prošireni oblik Vašeg rješenja nakon što riješite sustav!';
         document.getElementById('rjesenja').innerHTML = ''; 
         return 0; 
        }
    }
    
    if(isNaN(detr(matrix))==true){
        window.alert("Unijeli ste null matricu, pokušajte ponovno!");
        document.getElementById('dodatno').innerHTML = 'Ovdje će biti prikazan prošireni oblik Vašeg rješenja nakon što riješite sustav!';
        document.getElementById('rjesenja').innerHTML = '';
    }
    
    
    else if(detr(matrix) != 0  ){
        
        for(var i=0;i<table.rows.length-1;i++){
            
            dodatno[i]=' x' + br2 + ' = ' + ' D' + br2 +  '/' +  'D ' + ' = ' + determinants[i] + ' / ' + osnovna + ' = '  + result[i] + "<br>" ;
            result[i]='x' + br2  + '=' + result[i].toFixed(2);
            br2++;
        }
        for(let i=0;i<table.rows.length-1;i++){
        let dodatno2 = dodatno.join('');
        document.getElementById('dodatno').innerHTML = dodatno2;
        document.getElementById('rjesenja').innerHTML = result ;}}
        
        return 0;
        
    }
    
            
    
        
    





